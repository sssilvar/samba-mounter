#!/bin/env python3
import os
from os.path import join, isdir, normpath, basename
import subprocess

def get_var(var_name):
    value = os.getenv(var_name)
    if not value:
        return input(f'Insert {var_name}: ')
    return value

def mkdir(path):
    if not isdir(path):
        os.makedirs(path, exist_ok=True)

print('Insert Domain username/password:')
user = get_var('USER')
password = get_var('PASSWORD')

shared_folder_path = input('Insert shared folder path: ')
shared_folder_path = shared_folder_path.replace('\\', '/')

# Create local folder
local_folder = join(os.getenv('HOME'), 'Shared')
mkdir(local_folder)

# Create folder to share
mnt_folder = join(local_folder, basename(shared_folder_path))

# build command
# sudo mount -t cifs -o username=sssilvar,password=${PASSWD},workgroup=DANE,rw,vers=2.0,uid=$(id -u),gid=$(id -g),forceuid,forcegid,file_mode=0777,dir_mode=0777 //YCOLEYS/Productos_2020/PROSPECTIVA/SANTIAGO_SMITH_SILVA/Contrato_1289981_2020/ /home/sssilvar/Shared/cobro/
cmd = f"sudo mount -t cifs -o".split()
cmd += [f"username={user},password={password},workgroup=DANE,rw,vers=2.0,uid=$(id -u),gid=$(id -g),forceuid,forcegid,file_mode=0777,dir_mode=0777"]
cmd += [f'"{shared_folder_path}"', f'"{mnt_folder}"']

cmd_str = ' '.join(cmd)
# Execute it
if input(f'Command to execute:\n{cmd_str}\nConfirm (y/N)') == 'y':
    # Create folder to mount
    mkdir(mnt_folder)

    # Run mount command
    proc = subprocess.run(
        cmd,
        stdout=subprocess.PIPE,
        encoding="ascii",
    )
    print(proc.stdout)
print('Done!')
